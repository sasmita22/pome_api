<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/staff', function (Request $request) {
    return $request->staff();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'StaffController@login');
    Route::post('signup', 'StaffController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'StaffController@logout');
        Route::get('user', 'StaffController@staff');
    });
});